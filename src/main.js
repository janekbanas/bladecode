import '../node_modules/popper.js/dist/popper.min.js';
import '../node_modules/bootstrap';
import navComponent from './scripts/nav';

navComponent()

// Change Dots Navogation on CLICK and SCROLL

const links = document.querySelectorAll('.dot-single');
const main = document.querySelector('main');
const section = document.querySelectorAll('section');

function changeLinkState() {

    links.forEach((link) => link.classList.remove('active'));
    links.forEach((link) => link.classList.add('active'));


};

main.addEventListener('scroll', function() {
    changeLinkState(links);
}, false);

links.forEach((link) => link.addEventListener('click', function() {
    links.forEach((link) => link.classList.remove('active'));
    this.classList.add('active');
}));